# Odnośniki do grup i projektów
*Dokument ten został utworzony dla zachowania porządku na gitlab'ie*

- **Grupy**:
  
  - [Unity - projekty realizowane dla firmy](https://gitlab.com/unity-praca), grupa dla projektów wykonywanych komercyjnie. Ze względów na fakt, że projekty są komercyjne, to nie udostępniam do nich plików źródłowych, a jedynie indywidualne filmy z testów oraz publiczne artykuły. 
  - [Unity - przed rozpoczęciem pracy](https://gitlab.com/unity-przed-praca), grupa z kilkoma projektami, które zostały utworzone przed rozpoczęciem pracy komercyjnej;
  - [Inne assety](https://gitlab.com/pawel.babiuch0/inne-assety/-/blob/master/README.md), jakieś mniejsze paczuchy ułatwiające pracę przy wielu projektach.
  - [Java - sternik](https://gitlab.com/sternik-java), grupa do projektów utworzonych w trakcie kursu Sternik [CERTYFIKAT](https://drive.google.com/open?id=1Oadr_0opF08auK6722vYyCI8JOoI8OJn);

 Pozostałe projekty (nie licząc [inne-miniprojekty](https://gitlab.com/pawel.babiuch0/inne-miniprojekty) oraz [php-arena](https://gitlab.com/pawel.babiuch0/php-arena)) zostały utworzone w okresie przed podpięciem pracy komercyjnej.
